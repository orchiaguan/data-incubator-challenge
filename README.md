We're going to use a mail response data set from a real direct marketing campaign.

Each record represents an individual who was targeted with a direct marketing offer. 

The offer was a solicitation to make a charitable donation.

The columns (features) are:

Col.  Name      Description

----- --------- ----------------------------------------------------------------

1     income    household income

2     Firstdate data assoc. with the first gift by this individual

3     Lastdate  data associated with the most recent gift 

4     Amount    average amount by this individual over all periods (incl. zeros)

5     rfaf2     frequency code

6     rfaa2     donation amount code

7     pepstrfl  flag indicating a star donator

8     glast     amount of last gift

9     gavr      amount of average gift

10    class     one if they gave in this campaign and zero otherwise.


Our goal is to build a model to predict if people will give during the current campaign (this is the attribute called "class").